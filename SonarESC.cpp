/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <Arduino.h>
#include "common.h"
#include "utils.h"
#include "SonarESC.h"

#define TIMEOUT_US  70000

static SonarESC* SonarESC::mInstance(NULL);

static const PROGMEM u8 TBL_PINS_RX[] = {
    PIN_IN_SONAR_ECHO,
    PIN_IN_RCVR_PPM
};

SonarESC::SonarESC()
{
    memset(mStats, 0, sizeof(mStats));
    memset(mStartTicks, 0, sizeof(mStartTicks));
    memset(mRC, 0, sizeof(mRC));
    mLastPing = 0;
    mInstance = this;
}

s16 SonarESC::getRC(u8 ch)
{
    if (ch >= MAX_RC_CH)
        return RC_MIN_VALUE;

    if (mRC[ch] == 0) {
        return (ch == RC_CH_3) ? RC_MIN_VALUE : RC_MID_VALUE;
    }

    return constrain(mRC[ch], RC_MIN_VALUE, RC_MAX_VALUE);
}

void SonarESC::init(void)
{
    volatile u8 *reg;

    pinMode(PIN_OUT_SONAR_TRIG, OUTPUT);
    digitalWrite(PIN_OUT_SONAR_TRIG, LOW);

    for (u8 i = 0; i < MAX_PINS_USED; i++) {
        u8 pin = pgm_read_byte(&TBL_PINS_RX[i]);

        pinMode(pin, INPUT);

        // enable pin change interrupt
        reg = digitalPinToPCICR(pin);
        *reg |= _BV(digitalPinToPCICRbit(pin));

        // enable pin change mask
        reg = digitalPinToPCMSK(pin);
        *reg |= _BV(digitalPinToPCMSKbit(pin));

        mIntervals[i] = 0;
    }
}

void SonarESC::close(void)
{
    volatile u8 *reg;

    for (u8 i = 0; i < MAX_PINS_USED; i++) {
        u8 pin = pgm_read_byte(&TBL_PINS_RX[i]);

        // enable pin change mask
        reg = digitalPinToPCMSK(pin);
        *reg &= ~_BV(digitalPinToPCMSKbit(pin));
    }
}

void SonarESC::ping(void)
{
    if (mLastPing == 0 || (micros() - mLastPing) > TIMEOUT_US) {
        digitalWrite(PIN_OUT_SONAR_TRIG, HIGH);
        delayMicroseconds(10);
        digitalWrite(PIN_OUT_SONAR_TRIG, LOW);
        mLastPing = micros();
        for (u8 i = 0; i < MAX_SONAR_ECHO_CNT; i++)
            mStats[i] = FLAG_START;
    }
}

u8 SonarESC::getStatus(u8 idx)
{
    if (idx >= MAX_SONAR_ECHO_CNT)
        return FLAG_READY;

    if (mStats[idx] == FLAG_START) {
        if ((micros() - mLastPing) > TIMEOUT_US) {
            return FLAG_TIMEOUT;
        } else {
            return FLAG_WORKING;
        }
    }
    return mStats[idx];
}


u16 SonarESC::getDist(u8 idx)
{
    if (idx >= MAX_SONAR_ECHO_CNT)
        return 0;

    return mIntervals[idx] / 58;
}

void SonarESC::calcPeriod(u8 mask, u8 pins)
{
    u8  bv;
    u8  pin;

    for (u8 i = 0; i < MAX_PINS_USED; i++) {
        pin = pgm_read_byte(&TBL_PINS_RX[i]);
        bv  = digitalPinToBitMask(pin);

        if (mask & bv) {
            u32 ts = micros();

            if (pin == PIN_IN_RCVR_PPM && (pins & bv)) {
                mIntervals[i] = ts - mStartTicks[i];
                static u8   sCh;

                if (mIntervals[i] > 2500) {
                    sCh = 0;
                } else {
                    mRC[sCh++] = mIntervals[i];
                }
                mStartTicks[i] = ts;
            } else {
                if ((pins & bv)) {
                    mStartTicks[i] = ts;
                } else {
                    mIntervals[i] = ts - mStartTicks[i];
                    if (i < MAX_SONAR_ECHO_CNT)
                        mStats[i] = FLAG_FINISHED;
                }
            }
        }
    }
}

ISR(PCINT0_vect)
{
    u8  mask;
    u8  pins;
    static u8 ucLastPin;
    SonarESC *se = SonarESC::getInstance();

    pins      = PINB;
    mask      = pins ^ ucLastPin;
    ucLastPin = pins;
    se->calcPeriod(mask, pins);
}

