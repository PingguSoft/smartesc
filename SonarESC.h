/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _SONAR_ESC_H_
#define _SONAR_ESC_H_
#include <Arduino.h>
#include <avr/pgmspace.h>
#include "config.h"

#define FLAG_READY      0
#define FLAG_START      1
#define FLAG_WORKING    2
#define FLAG_FINISHED   3
#define FLAG_TIMEOUT    4

enum {
    RC_CH_1 = 0,
    RC_CH_2,
    RC_CH_3,
    RC_CH_4,
    RC_CH_5,
    RC_CH_6,
    RC_CH_7,
    RC_CH_8,
    MAX_RC_CH
};

#define RC_MIN_VALUE   900
#define RC_MID_VALUE   1500
#define RC_MAX_VALUE   2100


class SonarESC
{

public:
    SonarESC();
    ~SonarESC()  { close(); }

    void init(void);
    void close(void);

    void ping(void);
    u16  getDist(u8 idx);
    u8   getStatus(u8 idx);
    void reset(u8 idx) { mStats[idx] = FLAG_READY; }
    s16  getRC(u8 ch);
    void calcPeriod(u8 mask, u8 pins);
    static SonarESC* getInstance(void)  { return mInstance; }

private:
    u32 mLastPing;

    u8  mStats[MAX_SONAR_ECHO_CNT];
    s16 mIntervals[MAX_PINS_USED];
    u32 mStartTicks[MAX_PINS_USED];

    u16 mRC[MAX_RC_CH];

    static SonarESC* mInstance;

};

#endif
