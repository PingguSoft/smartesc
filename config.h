/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "common.h"


/*
*****************************************************************************************
* OPTIONS
*****************************************************************************************
*/
// board type


/*
*****************************************************************************************
* CONFIGURATION
*****************************************************************************************
*/
#define __FEATURE_DEBUG__           0
#define __STD_SERIAL__              1


/*
*****************************************************************************************
* RULE CHECK
*****************************************************************************************
*/

/*
*****************************************************************************************
* PINS
*****************************************************************************************
*/
#define MAX_SONAR_ECHO_CNT  1
#define MAX_RCV_PPM_CNT     1
#define MAX_PINS_USED       (MAX_SONAR_ECHO_CNT + MAX_RCV_PPM_CNT)


#if defined(__AVR_ATmega328P__)
    // sonar pins
    #define PIN_OUT_SONAR_TRIG      7
    #define PIN_IN_SONAR_ECHO       8

    // ppm receiver
    #define PIN_IN_RCVR_PPM         9

    // pwm outputs

    #define PIN_OUT_PWM_YAW         6
    #define PIN_OUT_PWM_THR         5
    #define PIN_OUT_PWM_AUX5        4
    #define PIN_OUT_PWM_AUX6        3
    #define PIN_OUT_PWM_AUX7        2
    #define PIN_OUT_PWM_AUX8        10


    #define PIN_OUT_LED             13

#elif defined(__AVR_ATmega32U4__)
    // sonar pins
    #define PIN_OUT_SONAR_TRIG      14
    #define PIN_IN_SONAR_ECHO       10

    // ppm receiver
    #define PIN_IN_RCVR_PPM         16

    // pwm outputs
    #define PIN_OUT_PWM_YAW         15
    #define PIN_OUT_PWM_THR         18
    #define PIN_OUT_PWM_AUX5        19
    #define PIN_OUT_PWM_AUX6        20
    #define PIN_OUT_PWM_AUX7        21
    #define PIN_OUT_LED             17
#else
    #error "Check board name !!!"
#endif



/*
*****************************************************************************************
* COMMON SETTINGS
*****************************************************************************************
*/



#endif
