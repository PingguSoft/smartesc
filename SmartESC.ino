#include <stdio.h>
#include <Servo.h>
#include "utils.h"
#include "SonarESC.h"

#define BRAKING_RC_VAL  1600

enum {
    SERVO_YAW = 0,
    SERVO_THR,
    SERVO_AUX5,
    SERVO_AUX6,
    SERVO_AUX7,
    SERVO_AUX8,
    SERVO_MAX
};


static SonarESC mESC;
static Servo    mServos[SERVO_MAX];

static const PROGMEM u8 TBL_PINS_PWM[] = {
    PIN_OUT_PWM_YAW,
    PIN_OUT_PWM_THR,
    PIN_OUT_PWM_AUX5,
    PIN_OUT_PWM_AUX6,
    PIN_OUT_PWM_AUX7,
    PIN_OUT_PWM_AUX8,
};

// create a FILE structure to reference our UART output function
static FILE uartout = {0} ;

// create a output function
// This works because Serial.write, although of
// type virtual, already exists.
static int uart_putchar (char c, FILE *stream)
{
    Serial.write(c) ;
    return 0 ;
}

void setup()
{
    Serial.begin(115200);

    // fill in the UART file descriptor with pointer to writer.
    fdev_setup_stream(&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
    // The uart is the standard output device STDOUT.
    stdout = &uartout ;

    mESC.init();
    for (u8 i = 0; i < sizeof(TBL_PINS_PWM); i++) {
        mServos[i].attach(pgm_read_byte(&TBL_PINS_PWM[i]), RC_MIN_VALUE, RC_MAX_VALUE);
    }
    pinMode(PIN_OUT_LED, OUTPUT);
}

static u32 dwLastTS = 0;
static u32 dwLastBraking = 0;
static s16 iLastThr = 1500;

#ifdef FEATURE_PID
float kP = 2;
float kI = 0.2;
float kD = 0.3;

static float inPrv  = 0;
static float errSum = 0;
static float errPrv = 0;


char *ftostr(float f, char *str)
{
    dtostrf(f, 4, 2, str);
    return str;
}


#define ITERM_MAX_ERROR         30          // 54Km/h
#define ITERM_MAX               8000

void resetPID(void)
{
    inPrv = 0;
    errSum = 0;
    errPrv = 0;
}

float doPID(float sp, float in, float dt)
{
    float err = sp - in;
    float output;

    err     = constrain(err, -ITERM_MAX_ERROR, ITERM_MAX_ERROR);
    errSum += (kI * err * dt);
    errSum  = constrain(errSum, -ITERM_MAX, ITERM_MAX);


//    output = kP * err + errSum  + (kD * (err - errPrv) / dt);
    output = kP * err + errSum  + (kD * (in - inPrv) / dt);

    char strP[7], strI[7], strD[7], strOut[7];
    LOG(PSTR("P:%s, I:%s, D:%s = %s"), ftostr(kP * err, strP), ftostr(errSum, strI), ftostr((kD * (in - inPrv) / dt), strD), ftostr(output, strOut));

    errPrv = err;
    inPrv  = in;

    return output;
}
#endif

static u8 led = 1;
static u8 cnt = 0;

void loop()
{
    u8      status = mESC.getStatus(0);
    u16     dist;
    s16     thr;
    u32     ts = millis();

    switch (status) {
        case FLAG_READY:
            mESC.ping();
            break;

        case FLAG_FINISHED:
        case FLAG_TIMEOUT:
            mESC.reset(0);
            break;
    }

    if (ts - dwLastTS > 20) {
        dist = mESC.getDist(0);

        if (cnt++ % 10 == 0) {
            led = !led;
            digitalWrite(PIN_OUT_LED, led);
        }


        for (u8 i = 0; i < 8; i++) {
            LOG(PSTR("%4d, "), mESC.getRC(i));
        }
        LOG(PSTR("    dist = %3d,   "), dist);

        thr = mESC.getRC(RC_CH_2);

        if (thr >= RC_MIN_VALUE) {
#ifdef FEATURE_PID
            if (Serial.available()) {
                u8 ch = Serial.read();

                if (ch == ' ') {
                    if (dwLastBraking == 0) {
                        dwLastBraking = millis();
                        LOG(PSTR("breaking start !!\n"));
                        resetPID();
                    } else {
                        dwLastBraking = 0;
                        LOG(PSTR("breaking finished !!\n"));
                    }
                }
            }
#endif
            if (mESC.getRC(RC_CH_8) > RC_MID_VALUE) {
                if (dwLastBraking == 0 && dist < 70 && iLastThr > BRAKING_RC_VAL) {
                    dwLastBraking = millis();
                    LOG(PSTR("breaking start !!\n"));
                    digitalWrite(PIN_OUT_LED, HIGH);
#ifdef FEATURE_PID
                    resetPID();
#endif
                } else if (dwLastBraking > 0 && iLastThr > BRAKING_RC_VAL && thr <= RC_MID_VALUE) {
                    dwLastBraking = 0;
                    LOG(PSTR("breaking finished !!\n"));
                    digitalWrite(PIN_OUT_LED, LOW);
                }
            }

            if (dwLastBraking > 0) {
#ifdef FEATURE_PID
                float out = doPID(50, dist, (ts - dwLastTS) * 0.001);
#endif
                thr = 1500 - (iLastThr - 1500);
            } else {
                iLastThr = thr;
            }

            LOG(PSTR("thr = %4d"), thr);

            mServos[SERVO_YAW].writeMicroseconds(mESC.getRC(RC_CH_1));
            mServos[SERVO_THR].writeMicroseconds(thr);
            mServos[SERVO_AUX5].writeMicroseconds(mESC.getRC(RC_CH_5));
            mServos[SERVO_AUX6].writeMicroseconds(mESC.getRC(RC_CH_6));
            mServos[SERVO_AUX7].writeMicroseconds(mESC.getRC(RC_CH_7));
            mServos[SERVO_AUX8].writeMicroseconds(mESC.getRC(RC_CH_8));
        }
        LOG(PSTR("\n"));
        dwLastTS = millis();
    }
}
